(define (domain vampire)
    (:requirements :conditional-effects)
    (:predicates
        (light-on ?r)
        (slayer-is-alive)
        (slayer-is-in ?r)
        (vampire-is-alive)
        (vampire-is-in ?r)
        (fighting)
        ;
        ; static predicates
        (NEXT-ROOM ?r ?rn)
        (CONTAINS-GARLIC ?r)
    )

    (:action toggle-light
        :parameters (?anti-clockwise-neighbor ?room ?clockwise-neighbor)
        :precondition (and
            (NEXT-ROOM ?anti-clockwise-neighbor ?room)
            (NEXT-ROOM ?room ?clockwise-neighbor)
            (not (fighting))
        )
        :effect
        (and
            (when
                (light-on ?room)
                (and
                    (vampire-is-alive)
                    (vampire-is-in ?room)
                )
                (and
                    (when
                        (NEXT-ROOM ?clockwise-neighbor ?room)
                        (and
                            (not (light-on ?room))
                            (not (slayer-is-in ?room))
                            (not (CONTAINS-GARLIC ?room))
                        )
                    )
                    (when
                        (NEXT-ROOM ?anti-clockwise-neighbor ?room)
                        (and
                            (not (light-on ?room))
                            (not (slayer-is-in ?room))
                            (not (CONTAINS-GARLIC ?room))
                        )
                    )
                )
            )
        )
    )

    (:action watch-fight
        :parameters (?room)
        :precondition (and
            (slayer-is-in ?room)
            (slayer-is-alive)
            (vampire-is-in ?room)
            (vampire-is-alive)
            (fighting)
        )
        :effect (and
;; Add your solution here.
        )
    )
)
